Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: pysword
Upstream-Contact: second@tgc.dk
Source: https://gitlab.com/tgc-dk/pysword

Files: *
Copyright: 2008-2019 Various developers:
 Kenneth Arnold, Joshua Gross, Tomas Groth, Ryan Hiebert, Philip Ridout, Matthew Wardrop
License: Expat

Files: debian/*
Copyright: 2016 Unit 193 <unit193@ubuntu.com>
           2016 Raoul Snyman <raoul@snyman.info>
           2019 Bastian Germann
License: Expat

Files: pysword/sapphire.py
Copyright: Dedicated to the Public Domain the author and inventor: (Michael Paul Johnson)
License: public-domain
 This is a direct port of the original C++ code to Python done by Tomas Groth in 2019.
 This port is also Public Domain.

Files: tests/resources/download_bibles.py
Copyright: 2008-2019 Various developers:
 Kenneth Arnold, Joshua Gross, Tomas Groth, Ryan Hiebert, Philip Ridout, Matthew Wardrop
License: GPL-2
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the Free
 Software Foundation; version 2 of the License.
 .
 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc., 51
 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public License version 2
 can be found in the file `/usr/share/common-licenses/GPL-2'.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
